import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatConversationsComponent } from './chat-conversations.component';

xdescribe('ChatConversationsComponent', () => {
  let component: ChatConversationsComponent;
  let fixture: ComponentFixture<ChatConversationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatConversationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatConversationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
