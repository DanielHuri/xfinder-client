import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';

import {Conversation} from '../classes/conversation';
import {ConversationService} from '../conversation/conversation.service';
import {TranslateService} from '../translate.service';

@Component({
  selector: 'chat-conversations',
  templateUrl: './chat-conversations.component.html',
  styleUrls: ['./chat-conversations.component.scss']
})
export class ChatConversationsComponent implements OnInit {

  conversations: Observable<Conversation[]>;
  cardDirectionRight: boolean;
  openedDefaultConversation: boolean; // Used to hide conversations list while default conversation is loading
  hideConversationsOnLoad: boolean;

  constructor(private conversationsService: ConversationService,
              private translateService: TranslateService) {
    this.cardDirectionRight = this.translateService.isFloatRight();
    this.conversations = conversationsService.allConversations;

    this.conversationsService.setDefaultConversation
      .subscribe(conversation => {
        if (conversation) {
          this.openedDefaultConversation = true;
        }
      });
  }

  ngOnInit(): void {
    // Check if the url contains userId
    const userId = new URLSearchParams(window.location.search).get('user_id');
    if (userId) {
      // Hide conversations list while default conversation is loading and default conversation is set
      this.hideConversationsOnLoad = true;

      this.conversationsService.setDefaultConversationId(userId);
    }
    this.conversationsService.loadInboxConversations('none');
  }

  loadMore() {
    this.conversationsService.loadMore();
  }
}
