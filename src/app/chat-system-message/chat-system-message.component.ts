import { Component, OnInit } from '@angular/core';
import {UserService} from '../user/user.service';
import {TranslateService} from '../translate.service';

@Component({
  selector: 'app-chat-system-message',
  templateUrl: './chat-system-message.component.html',
  styleUrls: ['./chat-system-message.component.scss']
})
export class ChatSystemMessageComponent implements OnInit {

  isInApplicationIframe: boolean;
  systemMessage: string;
  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.isInApplicationIframe = this.userService.isIframeInApplication();
    this.systemMessage = this.isInApplicationIframe ? 'APP_SYSTEM_MESSAGE_CHAT_CLEAR'
      : 'WEB_SYSTEM_MESSAGE_CHAT_CLEAR';
  }

}
