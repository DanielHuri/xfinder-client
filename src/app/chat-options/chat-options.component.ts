import {Component, OnInit, ViewChild} from '@angular/core';
import {ConversationService} from '../conversation/conversation.service';
import {TranslateService} from '../translate.service';
import {_document} from '@angular/platform-browser/src/browser';

@Component({
  selector: 'app-chat-options',
  templateUrl: './chat-options.component.html',
  styleUrls: ['./chat-options.component.scss']
})
export class ChatOptionsComponent implements OnInit {
  isFloatRight: boolean;
  checkAll = false;
  displayOptions = false;
  constructor(private conversationsService: ConversationService,
              private translateService: TranslateService) {

    this.conversationsService.displayChatOptions.subscribe(val => {
      this.displayOptions = val;
    });

    this.conversationsService.checkAll.subscribe(val => {
      this.checkAll = val;
      if (this.checkAll) {
        this.conversationsService.insertAllConversations();
      } else {
        this.conversationsService.removeAllConversation();
      }
      // this.allCheckBox.target.checked = val;
    });
    // this.conversationsService.setCheckAll$.subscribe(
    //   (val) => {
    //     this.checkedAll = val;
    //     if (val) {
    //       this.conversationsService.insertAllConversations();
    //     } else {
    //       this.conversationsService.clearSelectedConv();
    //     }
    //   }
    // );
  }

  ngOnInit() {
    this.isFloatRight = this.translateService.isFloatRight();
  }

  onCheckAll() {
    this.checkAll = !this.checkAll;
    this.conversationsService.checkAll.next(this.checkAll);
  }

  blockSelected() {
    this.conversationsService.blockSelectedUsers('add');
  }

  moveSelectedUsers() {
    this.conversationsService.moveSelectedUsers('trash');
  }

}
