import {Component, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Conversation} from '../classes/conversation';
import {ConversationService} from '../conversation/conversation.service';
import {LoggerService} from '../logger/logger.service';
import {MobileViewService} from '../mobile-view/mobile-view.service';
import {MessageService} from '../message/message.service';
import {UserService} from '../user/user.service';
import {User} from '../classes/user';
import {TranslateService} from '../translate.service';

@Component({
  selector: 'chat-conversation',
  templateUrl: './chat-conversation.component.html',
  styleUrls: ['./chat-conversation.component.scss']
})
export class ChatConversationComponent implements OnInit {

  showOption = false;
  selected = false;
  @Input() conversation: Conversation;
  @Output() conversationChecked: Conversation;
  @ViewChild('closeModal') myModal: any;
  @ViewChild('passToProfile') passToProfile: any;
  isInApplicationIframe: boolean;
  reportReasons;
  reportText;
  checkOn = false;
  currentUser: User;
  isDaWolf:boolean;
  arrowDirectionRight = this.translateService.isFloatRight();
  noRecentMessageText: string;
  newGeneralMessage: string; // In case user has no premium in order to view the content
  getUserProfileUrl_Reference;

  constructor(private conversationsService: ConversationService,
              private logger: LoggerService,
              private mobileView: MobileViewService,
              private messageService: MessageService,
              private userService: UserService,
              private translateService: TranslateService) {
    this.getUserProfileUrl_Reference  = this.conversationsService.getUserProfileUrl; // Html cant access function from service
    this.isInApplicationIframe        = this.userService.isIframeInApplication();

    this.conversationsService.currentConversation
      .subscribe(conversation => {
        this.selected = conversation.conversationUser.userId === this.conversation.conversationUser.userId;
      });

    this.userService.currentUser
      .subscribe(user => {
        this.currentUser = user;
      });

    this.conversationsService.checkAll.subscribe(
      (val) => {
        this.checkOn = val;
      }
    );
  }

  ngOnInit(): void {
    this.noRecentMessageText  = this.translateService.format('NO_RECENT_MESSAGES');
    this.newGeneralMessage    = this.translateService.format('NEW_GENERAL_MESSAGE');
    this.isDaWolf = this.isWolf();
  }

  conversationClicked(): void {
    this.conversationsService.setCurrentConversation(this.conversation);
    this.messageService.updateSocketIoConvRead(this.conversation.conversationUser.userId);
    this.mobileView.setIsOnConversation(false);
  }

  onClickOption() {
    this.showOption = !this.showOption;
  }

  linkToProfile() {
    if (!this.isInApplicationIframe) {
      this.passToProfile.nativeElement.click();
    }
  }

  oncheck(e) {
    this.checkOn = !this.checkOn;
    if (this.checkOn) {
      this.conversationsService.insertUserToSelected(this.conversation.conversationUser.userId);
    } else {
      this.conversationsService.removeUserFromSelected(this.conversation.conversationUser.userId);
    }
    e.cancelBubble = true;
    e.stopPropagation();

  }

  blockSingleUser() {
    this.conversationsService.blockSingleUser(this.conversation.conversationUser.userId);
  }

  deleteSingleUser() {
    this.conversationsService.moveSingleUser(this.conversation.conversationUser.userId);
  }

  getReasons() {
     this.conversationsService.getReportReasons()
       .then(data => {this.setReasons(data.reasons); });
  }

  setReasons(respone) {
    this.reportReasons = respone;
  }

  preformReport(reason) {
    if (reason === 'בחר סיבה') {
      // show "you must choose reason"

    } else {
      this.conversationsService.reportUser(this.conversation.conversationUser.userId ,
        this.reportReasons.find(function (element) {return element.label === reason; }).id ,
        this.reportText);
      this.myModal.nativeElement.click(); // click on the "close" button
      this.reportText = '';
    }
  }

  clearText() {
    this.reportText = '';
  }


  isWolf(){
    return this.conversation.is_wolf;
  }
}