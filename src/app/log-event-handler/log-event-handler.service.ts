import {Injectable} from '@angular/core';
import {webSocketEvents, WebsocketService} from '../websocket/websocket.service';
import {UserService} from '../user/user.service';
import {LoggerService} from '../logger/logger.service';

@Injectable()
export class LogEventHandlerService {

  constructor(private userService: UserService,
              private websocketService: WebsocketService,
              private logger: LoggerService) {
  }

  sendLogEvent(event) {
    const user = this.userService.currentUserData;
    const eventToLog = {event: webSocketEvents.logEvent, args: {user: user, event: event}};
    this.logger.info('sendLogEvent from LogEventHandlerService', eventToLog);
    this.websocketService.getSocket()
      .next(eventToLog);
  }

  logInfo(...params) {
    this.sendLogEvent(params);
  }

  logError(err) {
    this.sendLogEvent(err);
  }

}
