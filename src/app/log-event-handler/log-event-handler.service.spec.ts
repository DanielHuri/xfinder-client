import { TestBed, inject } from '@angular/core/testing';

import { LogEventHandlerService } from './log-event-handler.service';

describe('LogEventHandlerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LogEventHandlerService]
    });
  });

  it('should be created', inject([LogEventHandlerService], (service: LogEventHandlerService) => {
    expect(service).toBeTruthy();
  }));
});
