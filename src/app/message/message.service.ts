import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {ConversationService} from '../conversation/conversation.service';
import {ServerService, urls} from '../server/server.service';
import {webSocketEvents, WebsocketService} from '../websocket/websocket.service';
import {UserService} from '../user/user.service';
import {LoggerService} from '../logger/logger.service';
import {Message} from '../classes/message';
import {User} from '../classes/user';
import * as _ from 'lodash';
import {Observable} from 'rxjs/Observable';
import * as moment from 'moment';
import * as Rx from 'rxjs/Rx';

@Injectable()
export class MessageService {

  // Current conversation messages
  private cache: {
    conversationUserId: string,
    page: number,
    hasMore?: boolean,
    maxPage?: number,
    messages: Message[]
  };
  currentConversationsMessages: BehaviorSubject<Message[]> = new BehaviorSubject([]);

  // Messages from server, socket.io and client
  messagesByUserId: Subject<any> = new Subject();

  prefromDetectChanges: Subject<any> = new Subject();

  // Current user from UserService
  currentUser: User;

  // Typing situation
  isTyping;
  // Message read
  convRead: Subject<boolean> = new Subject();

  constructor(private conversationService: ConversationService,
              private serverService: ServerService,
              private websocketService: WebsocketService,
              private userService: UserService,
              private logger: LoggerService) {
    // this.unreadMessages =
    //   Observable.combineLatest(this.conversationService.allConversations, this.conversationService.currentConversation,
    //     (allConversations, currentConversation) => {
    //       return allConversations.reduce((acc, current) => {
    //         const reduceCurrentConversationUser = current.conversationUser;
    //         if (reduceCurrentConversationUser) {
    //           if (currentConversation && currentConversation.conversationUser) {
    //             acc += (!current.isRead &&
    //               reduceCurrentConversationUser.userId !== currentConversation.conversationUser.userId ) ? 1 : 0;
    //           } else {
    //             acc += current.isRead ? 0 : 1;
    //           }
    //         }
    //         return acc;
    //       }, 0);
    //     });

    this.userService.currentUser
      .subscribe(user => {
        this.currentUser = user;
      });

    // Subscribe for getting the currently conversation in the app
    this.subscribeToCurrentConversation();
    this.subscribeToMessagesByUserId();
    this.subscribeToWebSocket();
  }

  private checkCache(conversationUserId) {
    this.logger.debug('checkCache, conversationUserId:', conversationUserId, 'this.cache:', this.cache);
    if (this.cache && this.cache.conversationUserId && this.cache.messages) {
      const bConversationUserId = this.cache.conversationUserId.toString() === conversationUserId.toString();
      this.logger.debug(`this.cache.conversationUserId === conversationUserId: ${bConversationUserId}`);
      return bConversationUserId;
    }
    return false;
  }

  private checkCacheTyping(conversationUserId) {
    this.logger.debug('checkCache, conversationUserId:', conversationUserId, 'this.cache:', this.cache);
    if (this.cache && this.cache.conversationUserId) {
      const bConversationUserId = this.cache.conversationUserId === conversationUserId;
      this.logger.debug(`this.cache.conversationUserId === conversationUserId: ${bConversationUserId}`);
      return bConversationUserId;
    }
    return false;
  }

  private subscribeToCurrentConversation() {
    this.conversationService.currentConversation
      .filter(conversation => !!(conversation.conversationUser && conversation.conversationUser.userId))
      .filter((conversation) => {
        return !this.cache || this.cache.conversationUserId !== conversation.conversationUser.userId;
      })
      .subscribe(conversation => {
        const userId = conversation.conversationUser.userId;
        this.cache = {
          page: 1,
          conversationUserId: userId,
          messages: [],
          hasMore: true,
        };
        this.nextOnCurrentConversationMessages();
        this.getMessagesByUserId();
        // this.loadMore(this.cache.conversationUserId);
      });
  }

  private getMessagesByUserId() {
    const conversationUserId = this.cache.conversationUserId;
    return this.serverService.sendGetRequest(urls.conversation.byUserId(conversationUserId, this.cache.page))
      .then(data => {
        this.cache.maxPage = data['max_page'];
        const conversationUser = this.userService.convertUserResponse(data['conversation_user']);
        const viewingUser = this.userService.convertUserResponse(data['viewing_user']);
        return {data, conversationUser, viewingUser};
      })
      .then(({data, conversationUser, viewingUser}) => {
        return this.convertMessagesFromResponse(data, conversationUser, viewingUser);
      })
      .then((data) => {
        this.messagesByUserId.next({conversationUserId: conversationUserId, messages: data.messages});
        this.cache.page++;      // Increase page number
        // Load all pages if current page isn't the 1
        if (this.cache.page > 2 && this.cache.page <= this.cache.maxPage) {
          this.getMessagesByUserId();
        }
        return data;
      })
      .catch(err => {
        this.logger.error('Error in get messages -> ', err);
      });
  }

  private convertMessagesFromResponse(responseData, conversationUser, viewingUser) {
    responseData.messages = responseData.messages
      .map(message => this.convertMessageResponse(message, conversationUser, viewingUser));
    return responseData;
  }

  private convertMessageResponse(response, conversationUser, viewingUser) {
    const message = new Message();
    message.messageId = response.message_id;
    message.userId = response.user_id;
    message.dateRead = response.date_read;
    message.content = response.content;
    message.dateCreated = response.date_created;
    message.isViewingUserMessage = !!response.is_viewing_user_message;
    message.successfulySent = true;
    message.isRead = !!response.date_read;
    message.prevMsgDiffUser = false;

    if (response.images) {
      message.image = response.images[0];
    }

    if (message.isViewingUserMessage) {
      message.user = viewingUser;
    } else {
      message.user = conversationUser;
    }

    this.logger.verbose('response from convertMessageResponse', response);
    this.logger.verbose('conversation from convertMessageResponse', message);

    return message;
  }

  private subscribeToMessagesByUserId() {
    this.messagesByUserId
      .filter(({conversationUserId}) => {
        return this.checkCache(conversationUserId);
      })
      .subscribe(({messages}) => {
        this.pushMessagesToCache(messages);
      });
  }

  private pushMessagesToCache(messages: Message[]) {
    this.logger.debug('pushMessagesToCache ---- message:', messages);
    this.logger.debug('this.cache before', this.cache);
    this.cache.messages.unshift(...messages);
    this.sortCacheMessages();
    this.setPrevMsgSameUser();
    this.logger.debug('this.cache after', this.cache);
    this.nextOnCurrentConversationMessages();
  }

  private setPrevMsgSameUser() {
    // Variable Definition
    const length = this.cache.messages.length;

    // Code Section
    if (length > 0) {
      this.cache.messages[0].prevMsgDiffUser = true;
    }

    for (let i = 1; i < length; i++) {
      this.cache.messages[i].prevMsgDiffUser = this.cache.messages[i - 1].isViewingUserMessage !==
        this.cache.messages[i].isViewingUserMessage;
    }
  }
  private sortCacheMessages() {
    this.cache.messages =  _.sortBy(this.cache.messages, 'dateCreated');
  }

  private subscribeToWebSocket() {
    this.websocketService.getSocket()
      .filter(data => {
        const b = data.event === webSocketEvents.newMessage;
        this.logger.debug(`filter 1: data.event: ${data.event} && data.event === 'message':${b}`);
        return b;
      })
      .map(data => {
        this.logger.debug('map 2: data:', data);
        return data.data;
      })
      .filter(data => {
        data.fromUid = data.fromUid.toString();
        // Remove this later and check the server response parameters
        data.isViewingUserMessage = false;
        const result = this.checkCache(data.fromUid);
        this.conversationService.increaseTotalMessages(data.fromUid);
        this.conversationService.updateConversationOnSendMsg(data, data.fromUid, !result);
        return result;
      })
      .subscribe(newMessage => {
        this.logger.debug('data from webSocket after filters', newMessage);
        // Send to server message read
        this.conversationService.setConversationRead(this.cache.conversationUserId);
        // Update socket IO conv read
        this.updateSocketIoConvRead(this.cache.conversationUserId);
        newMessage.image = newMessage.images[0];
        this.pushSingleMessageToCache(newMessage);
      });

    // Subscribe to typing event from socket IO
    // the typing will be updated only when the observable pass all the filters successfully
    this.isTyping = this.websocketService.getSocket()
      .filter(data => {
        const b = data.event === webSocketEvents.typing;
        this.logger.debug(`filter 1: data.event: ${data.event} && data.event === 'typing':${b}`);
        return b;
      })
      .map(data => {
        return data.data;
      })
      .filter(data => {
        return this.checkCacheTyping(data.fromId);
      })
      .map(data => data.isTyping) // The output of the map is the input for this.typing
      .switchMap((isTyping) => { /* switchMap - map to inner observable and emits value */
        /* Merge both observables , emits true and after 600 false */
        return Rx.Observable.merge(Rx.Observable.of(isTyping),
          Rx.Observable.timer(600).mapTo(false));
      });

    // Subscribe to read msg event from socket IO
    this.websocketService.getSocket()
      .filter(data => {
        const b = data.event === webSocketEvents.convRead;
        this.logger.debug(`filter 1: data.event: ${data.event} && data.event === 'convRead':${b}`);
        return b;
      })
      .map(data => {
        return data.data;
      })
      .filter(data => {
        this.conversationService.updateConversationOnReadMsg(data.fromUid);
        return this.checkCache(data.fromUid);
      })
      .subscribe((data) => {
        this.setCurrentConversationMsgRead();
        // this.sortAndUpdateLast(data.fromUid);
      });
  }

  public sendMessage(toUserId, message) {
    if (this.checkCache(toUserId)) {
      this.pushSingleMessageToCache(message);
      this.conversationService.increaseTotalMessages(toUserId);
    }

    const body = new URLSearchParams();
    body.set('content', message.content);
    body.set('tempId', message.tempId);
    if ( message.image) {
      body.set('imageIds[]', message.image.id);
    }
    this.conversationService.updateConversationOnSendMsg(message, toUserId, false);
    return this.serverService.sendPostRequest(urls.post.saveMessage(toUserId), body)
      .then(data => {
        const find = _.find(this.cache.messages, {tempId: message.tempId});
        if (find) {
          find.successfulySent = true;
          this.prefromDetectChanges.next(message.tempId);
          this.conversationService.updateConversationOnSendMsg(find, toUserId, false);
        }
        return data;
      })
      .catch(err => {
        this.logger.error('error in saveMessage post request', err);
        throw err;
      });
  }

  private pushSingleMessageToCache(message: Message) {
    this.logger.debug('pushSingleMessageToCache ---- message:', message);
    this.logger.debug('this.cache before', this.cache);
    message.prevMsgDiffUser = this.cache.messages.length === 0 ||
      this.cache.messages[this.cache.messages.length - 1].isViewingUserMessage !== message.isViewingUserMessage;
    this.cache.messages.push(message);
    this.logger.debug('this.cache after', this.cache);
    // this.conversationService.activeConversations.isActive = true;
    this.nextOnCurrentConversationMessages();
  }

  private nextOnCurrentConversationMessages() {
    this.currentConversationsMessages.next(this.cache.messages);
  }

  public loadAll(conversationUserId) {
    console.log('check cache in load more');
    if (this.checkCache(conversationUserId) && this.cache.page <= this.cache.maxPage) {
      console.log('loading more');
      this.getMessagesByUserId();
    }
  }

  public setTyping(otherSideId, val) {
    if (this.checkCacheTyping(otherSideId)) {
      // Send to socket io current typing situation
      this.websocketService.getSocket()
        .next({event: webSocketEvents.typing, args: {fromId: this.currentUser.userId, toId: otherSideId, isTyping: val}});
    }
  }

  public typingOffNewConversation(conversationUserId) {
    this.websocketService.getSocket()
      .next({event: webSocketEvents.typing, args: {fromId: conversationUserId, toId: this.currentUser.userId, isTyping: false}});
  }

  public updateSocketIoConvRead(conversationId) {
    if (!this.conversationService.getLastMessageMyFromConv(conversationId)) {
      this.websocketService.getSocket()
        .next({event: webSocketEvents.convRead, args: {fromUid: this.currentUser.userId, toId: conversationId, isRead: true}});
    }

  }

  private setCurrentConversationMsgRead() {
    // this.conversationService.setAllMessagesAsRead(this.cache.conversationUserId);
    this.cache.messages.forEach((message) => {
      if (!message.isRead) {
        message.isRead = true;
        message.dateRead = moment().unix();
      }
    });
    this.currentConversationsMessages.next(this.cache.messages);
  }

  public getHasMore() {
    return this.cache.page <= this.cache.maxPage;
  }

  public currentConversationClosed() {
    this.cache = null;
  }
}
