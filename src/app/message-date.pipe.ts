import { Pipe, PipeTransform } from '@angular/core';
import {TranslateService} from './translate.service';
// import moment = require('moment');
import * as moment from 'moment';
import {Languages} from './languages.enum';



@Pipe({
  name: 'messageDate'
})
export class MessageDatePipe implements PipeTransform {
  private lang: Languages;
  constructor(private translateService: TranslateService) {
    this.lang = this.translateService.getCurrentLang();
  }
  transform(value: any, args?: any): any {
    value *= 1000;
    if ( moment(moment().unix() * 1000).format('YYYY-MM-DD')  === moment(value).format('YYYY-MM-DD') ) {
      return moment(value).format('H:mm');
    }
    moment.locale(this.lang.toString());
    return moment(value).format('H:mm | D MMMM');
  }
}
