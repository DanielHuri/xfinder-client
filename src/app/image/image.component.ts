import {AfterViewInit, ChangeDetectorRef, Component, Input, OnInit, ViewChild} from '@angular/core';
import {Image} from '../image';
import {ImageGalleryComponent} from '../image-gallery/image-gallery.component';
import {GalleryService} from '../gallery.service';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements AfterViewInit {

  @Input() image: Image;
  @ViewChild('userImage') userImage: any;
  selected = false;
  imageSuccess = true;

  constructor(private gallery: ImageGalleryComponent,
              private galleryService: GalleryService,
              private ref: ChangeDetectorRef) {

    // this.galleryService.selectedImg
    //   .subscribe(image => {
    //     if (this.imageSuccess) {
    //       this.selected = this.image && image && this.image.id === image.id ;
    //       this.ref.detectChanges();
    //     }
    //   });

    this.galleryService.selectedImgs.subscribe(imgs=>{
      let found = false;
      for(let i=0; i<imgs.length && !found;i++){
        found = this.image && imgs[i] && this.image.id === imgs[i].id;
      }
      this.selected = found;
      this.ref.detectChanges();
    });

    this.galleryService.imageStatus
      .filter(data => {
        return this.image && this.image.id === data.tempId;
      })
      .subscribe(data => {
        this.changeImageStatus(data);
      });

  }

  ngAfterViewInit() {
    if (this.image) {
      this.userImage.nativeElement.style.content = 'url(' + this.image.url + ')'; // Paste image dynamically
    }
  }

  imageClicked() {
    if (this.imageSuccess) {
      if (this.selected) { // in case image already selected
        let tempimg = new Image();
        tempimg.id = this.image.id*-1;
        this.galleryService.selectedImg.next(tempimg); // un-select image
      } else {
        this.galleryService.selectedImg.next(this.image);
      }
    }
  }

  changeImageStatus(data) {
    switch (data.status) {
      case 'success':
        // Change the img url and the div url
        this.userImage.nativeElement.style.content = 'url(' + data.image.url + ')';
        console.log('in success');
        this.image = data.image;
        this.imageSuccess = true;

        // If this is the last image that uploaded, select this image
        if (data.lastImage) {
          this.imageClicked();
        }
        break;
      case 'loading':
        // Change the div url
        this.userImage.nativeElement.style.content = 'url(' + data.image.url + ')';
        this.imageSuccess = false;
        break;
      case 'fail':
        // Change the img url and the div url
        this.image.id = -100;
        this.userImage.nativeElement.style.content = 'url(' + this.image.url + ')';
        this.userImage.nativeElement.style.opacity = 0.2;
        this.imageSuccess = false;
        break;
    }
  }

}
