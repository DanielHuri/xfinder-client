import {Injectable} from '@angular/core';
import {ReplaySubject, Subject} from 'rxjs';
import {ServerService, urls} from '../server/server.service';
import {webSocketEvents, WebsocketService} from '../websocket/websocket.service';
import {LoggerService} from '../logger/logger.service';
import {User} from '../classes/user';

/**
 * UserService manages our current user
 */
@Injectable()
export class UserService {

  currentUser: Subject<User> = new ReplaySubject(); // Pass currentUser observable
  currentUserData: User;                            // Hold currentUser data

  constructor(private serverService: ServerService,
              private logger: LoggerService,
              private websocketService: WebsocketService) {
  }

  initCurrentUser() {
    return this.serverService.sendGetRequest(urls.currentUser.getCurrent)
      .then(data => this.convertUserResponse(data))
      .then(data => {
        this.logger.verbose('initCurrentUser - data from current user', data);
        return data;
      })
      .then(user => {
        this.currentUserData = user;
        this.currentUser.next(user);
        this.subscribeWebSocketGetUid();
        return this.currentUser;
      });
  }

  // Listen to uid requests
  subscribeWebSocketGetUid() {
    this.websocketService.getReplaySocket()
      .filter(data => {
        const status = data.event === webSocketEvents.getUid;
        this.logger.debug(`filter 1: data.event: ${data.event} && data.event === 'getUid':${status}`);
        return status;
      })
      .filter( () => {
        return this.currentUserData !== undefined;
      })
      .subscribe( () => {
        this.websocketService.connectWithUid(this.currentUserData.userId); // Send uid to socket IO
      });
  }

  convertUserResponse(userResponse) {
    const converted = new User();
    if (userResponse) {
      converted.userId = userResponse.user_id;
      converted.online = userResponse.online;
      converted.nickName = userResponse.nickname;
      converted.age = userResponse.age;
      converted.city = userResponse.city;
      converted.logo = userResponse.logo;
      converted.needsPremium = userResponse.needs_premium;
      converted.subtype = userResponse.user_subtype;
      converted.type = userResponse.user_type;
      converted.lookingUserType = userResponse.looking_user_type;
      converted.country = userResponse.country;
      converted.lang = userResponse.lang;
      converted.phoneNumber=userResponse.phone_number;
    } else {
      this.logger.warn('userResponse not exists', userResponse);
    }
    return converted;
  }

  isIframeInApplication() {
    return new URLSearchParams(window.location.search).get('is_app') === 'true';
  }


}
