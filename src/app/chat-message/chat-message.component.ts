import {AfterViewInit, ChangeDetectorRef, Component, Input, ViewChild} from '@angular/core';
import {Message} from '../classes/message';
import {ChatWindowComponent} from '../chat-window/chat-window.component';
import {MessageService} from '../message/message.service';
import {TranslateService} from '../translate.service';
import {UserService} from '../user/user.service';
import {ConversationService} from '../conversation/conversation.service';

@Component({
  selector: 'chat-message',
  templateUrl: './chat-message.component.html',
  styleUrls: ['./chat-message.component.scss']
})
export class ChatMessageComponent implements AfterViewInit {

  @Input() message: Message;
  @ViewChild('messageBubble') messageBubble: any;
  @ViewChild('profileLinkBtn') profileLinkBtn: any;
  isInApplicationIframe: boolean;
  isFloatRight: boolean;
  getUserProfileUrl_Reference;
  constructor(private  chatWindowComp: ChatWindowComponent,
              private  messageService: MessageService,
              private  ref: ChangeDetectorRef,
              private  translationService: TranslateService,
              private  userService: UserService,
              private  conversationsService: ConversationService) {
    this.isFloatRight                 = this.translationService.isFloatRight();
    this.isInApplicationIframe        = this.userService.isIframeInApplication();
    this.getUserProfileUrl_Reference  = this.conversationsService.getUserProfileUrl; // Html cant access function from service

    this.messageService.prefromDetectChanges
      .subscribe(msgId => {
        if (this.message.tempId === msgId) {
          this.ref.detectChanges();
        }
      });
  }

  openImage() {
    this.chatWindowComp.openImage(this.message.image.url);
  }

  ngAfterViewInit() {

      if (this.message.image) {
        this.messageBubble.nativeElement.style.maxHeight =  '250px';
        this.messageBubble.nativeElement.style.maxWidth =  '80%';
        this.messageBubble.nativeElement.style.minHeight =  '100px';
        this.messageBubble.nativeElement.style.minWidth =  '110px';
        this.messageBubble.nativeElement.style.height = this.message.image.height + 'px';
        this.messageBubble.nativeElement.style.width = this.message.image.width + 'px';
        this.messageBubble.nativeElement.style.padding =  '1px';
        this.messageBubble.nativeElement.style.position =  'relative';
        this.messageBubble.nativeElement.style.backgroundColor = 'unset';
        this.messageBubble.nativeElement.style.marginBottom = '10px';
      }
      if (this.isRTL(this.message.content)) {
        this.messageBubble.nativeElement.style.textAlign = 'right';
      }


  }

  isRTL(s) {
    const ltrChars        = 'A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02B8\u0300-\u0590\u0800-\u1FFF' +
      '\u2C00-\uFB1C\uFDFE-\uFE6F\uFEFD-\uFFFF',
      rtlChars        = '\u0591-\u07FF\uFB1D-\uFDFD\uFE70-\uFEFC',
      rtlDirCheck     = new RegExp('^[^' + ltrChars + ']*[' + rtlChars + ']');
    return rtlDirCheck.test(s);
  };

  linkToProfile() {
    if (!this.isInApplicationIframe) {
      this.profileLinkBtn.nativeElement.click();
    }
  }
}
