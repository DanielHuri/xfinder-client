import {Injectable} from '@angular/core';
import * as moment from 'moment';

@Injectable()
export class LoggerService {

  constructor() {
  }

  verbose(...params) {
    console.log(moment().format('HH:mm:ss'), ...params);
  }

  debug(...params) {
    console.log(moment().format('HH:mm:ss'), ...params);
  }

  info(message, ...params) {
    console.log(message, ...params);
  }

  warn(message, ...params) {
    console.log(message, ...params);
  }

  error(message, err) {
    console.error(message, err || '');
  }
}
