export class Folders {
  static INBOX = 'inbox';
  static OUTBOX = 'outbox';
  static TRASH = 'trash';
  static FILTER_NONE = 'none';
  static FILTER_UNREAD = 'unread';
}
