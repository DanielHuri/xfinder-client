import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {ServerService, urls} from '../server/server.service';
import {LoggerService} from '../logger/logger.service';
import {Conversation} from '../classes/conversation';
import {Folders} from './folders.enum';
import * as _ from 'lodash';
import {UserService} from '../user/user.service';
import * as moment from 'moment';
import {TranslateService} from '../translate.service';

declare let $: any;
@Injectable()
export class ConversationService {

  // All conversations
  private cache: {
    folder: Folders,
    page: number,
    filter?: string,
    hasMore?: boolean,
    maxPage?: number,
    conversations: Conversation[],
  };

  activeConversations: {
    unReadMessages: number,
    isActive: boolean,
    prevCurrentId: string,
    counterOn: boolean,
    conversations: Conversation[],
  };
  private tempPage; // used to update conversations after blacklist
  private isConversationFromUrlParams: boolean; // Conversation user id is passed via url params
  private selectedUsers = [];
  allConversations: BehaviorSubject<Conversation[]> = new BehaviorSubject([]);
  convId;
  defaultConversationId ;
  public checkAll = new Subject<any>();
  public pushSystemMessage = new Subject<boolean>(); // Display message when no conversations
  public displayChatOptions = new Subject<boolean>();
  // New conversations
  private newConversationsSubject: Subject<any> = new Subject();
  isLoadingPage = false;
  // Current conversations
  currentConversation: Subject<Conversation> = new Subject();
  newMsgNotification: Subject<Conversation> = new Subject();
  activeConvs: BehaviorSubject<Conversation[]> = new BehaviorSubject([]);
  closeCurrentConv: Subject<boolean> = new Subject();
  setDefaultConversation: Subject<Conversation> = new Subject();

  constructor(private serverService: ServerService,
              private logger: LoggerService,
              private userService: UserService,
              private translateService: TranslateService) {

    this.activeConversations = {
      unReadMessages: 0,
      isActive: false,
      conversations: [],
      prevCurrentId: '-1',
      counterOn: false,
    };
    // new conversations logic
    this.newConversationsSubject
      .subscribe(conversations => {
        this.cache.conversations.push(...conversations);
        this.allConversations.next(this.cache.conversations);
      });

    // this.lastConversations = this.activeConvs;
  }


  loadMore() {
    if (this.cache.hasMore && !this.isLoadingPage) {
      this.isLoadingPage = true;
      this.checkAll.next(false);
      return this.loadConversationsByFolder(this.cache.folder, this.cache.filter);
    }
    return null;
  }

  loadInboxConversations(filter) {
    return this.loadConversationsByFolder(Folders.INBOX, filter);
  }

  loadOutboxConversations(filter) {
    return this.loadConversationsByFolder(Folders.OUTBOX, filter);
  }

  loadTrashConversations() {
    return this.loadConversationsByFolder(Folders.TRASH, Folders.FILTER_NONE);
  }

  loadConversationsByFolder(folder: Folders, filter) {
    // Clear cache if needed
    if (!this.cache || this.cache.folder !== folder) {
      this.cache = {
        folder: folder,
        page: 1,
        filter: Folders.FILTER_NONE,
        conversations: []
      };
    } else if (this.cache.filter !== filter) {
      this.cache = {
        folder: folder,
        page: 1,
        filter: filter,
        conversations: []
      };
    }
    // Send request with the currentPageParameter
    return this.serverService.sendGetRequest(urls.conversations.byFolder(this.cache.folder, this.cache.page, this.cache.filter))
      .then(data => {
        this.isLoadingPage = false;
        this.logger.debug('data from get', data);
        this.cache.page++;
        this.cache.hasMore = data.has_more;
        this.cache.maxPage = data.max_page;
        return data;
      })
      .then(data => {
        return data.conversations.map(conversation => {
          return this.convertConversation(conversation);
        }).filter( newConversation => {return this.cache.conversations.findIndex
        (conversation => conversation.conversationUser.userId === newConversation.conversationUser.userId) === -1; } );
      })
      .then(conversations => {
        this.newConversationsSubject.next(conversations); // Push conversations

        // this.pushSystemMessage.next(conversations.length === 0 ); // Push default system message
        // ? At 6/6/2019 we've asked to hide to default ssytem message
        this.pushSystemMessage.next(false); // Push default system message

        // Set default conv in case of - desktop + first conversation page + no conv id passed in url
        if (!this.defaultConversationId && this.cache.page === 2 && $(window).width() >= 700) {
          this.defaultConversationId = this.getDefaultConversation(conversations);
        }
        if (this.defaultConversationId) {
          this.logger.debug('loading default conversation with id ' , this.defaultConversationId);
          this.getConversationById(conversations, this.defaultConversationId);
          this.defaultConversationId = null;
        }
      });
  }

  loadConversationById(userId) {
    // Send request with the currentPageParameter
    this.serverService.sendGetRequest(urls.conversations.byFolder(this.cache.folder, this.cache.page, this.cache.filter))
      .then(data => {
        return data.conversations.map(conversation => {
          return this.convertConversation(conversation);
        });
      })
      .then(conversations => {
        // Pass the conversation to the called function
      });
  }

  convertConversation(response) {
    const conversation = new Conversation();
    conversation.isRead = response.is_read;
    conversation.logo = response.logo;
    conversation.isRead = !!response.is_read;
    conversation.name = response.name;
    conversation.user_one = response.user_one;
    conversation.user_two = response.user_two;
    conversation.lastMessageMy = !!response.last_message_my;
    conversation.lastMessageSentTime = response.last_message_sent_time;
    conversation.lastMessageReadime = response.last_message_read_time;
    conversation.lastMessageRead = !!response.last_message_read_time;
    conversation.totalMessages = response.total_messages;
    conversation.lastMessageSent = true;
    conversation.last_message_content = response.last_message_content ? response.last_message_content.substring(0,38)+((response.last_message_content.length > 38)?"...":""): this.translateService.format('NO_RECENT_MESSAGES');
    conversation.conversationUser = this.userService.convertUserResponse(response.conversation_user);
    conversation.conversationUser.nickName = response.conversation_user.nickname;
    conversation.conversationUser.totalNotes = response.conversation_user.total_notes;
    conversation.is_wolf = response.is_wolf === "1";
    this.logger.verbose('response from convertConversation', response);
    this.logger.verbose('conversation from convertConversation', conversation);

    return conversation;
  }

  setCurrentConversation(conversation: Conversation) {
    // if (this.activeConversations.isActive) {
    //   // Insert last conversation to active
    //   this.activeConversations.prevCurrentId = this.convId;
    //   this.pushNewActiveConversation(this.convId);
    //   this.activeConversations.isActive = false;
    // }
    this.convId = conversation.conversationUser.userId;
    // Remove current conversation from active
    this.popConversationFromActive(this.convId, conversation);
    this.currentConversation.next(conversation);
    if (conversation && conversation.conversationUser) {
      this.setConversationRead(conversation.conversationUser.userId);
    }
  }

  setConversationRead(conversationUserId) {
    const conversations = this.cache.conversations;
    const find = _.findIndex(conversations, {conversationUser: {userId: conversationUserId}});
    if (find >= 0 && !conversations[find].lastMessageRead && !conversations[find].lastMessageMy) {
      conversations[find].isRead = true;
      conversations[find].lastMessageRead = true;
      conversations[find].lastMessageReadime = moment().unix();
      this.allConversations.next(conversations);
    }
    return this.serverService.sendPutRequest(urls.put.setConversationRead(conversationUserId), {});
  }

  blockSelectedUsers(action) {
    // Send put request with the current action
    const body = this.decode();
    return this.serverService.sendPutRequest(urls.put.updateBlockList(action), body)
      .then( success => {
        if (success) {
          this.moveSelectedUsers('trash');
        }
        return success; // return the result (success/error/data)
      });
  }

  blockSingleUser(id) {
    // Send put request with the current action
    const body = 'userIds[]=' + id;
    return this.serverService.sendPutRequest(urls.put.updateBlockList('add'), body)
      .then(success => {
        if (success) {
          this.moveSingleUser(id);
        }
        return success; // return the result (success/error/data)
      });
  }

  moveSingleUser(id) {
    const body = 'userIds[]=' + id + '&folder=trash';
    return this.serverService.sendPutRequest(urls.put.moveConversations(), body)
      .then( success => {
        if (success) {
          this.removeUser(id);
        }
        return success; // return the result
      });
  }

  moveSelectedUsers(destinationFolder) {
    const body = this.decode() + '&folder=' + destinationFolder;
    return this.serverService.sendPutRequest(urls.put.moveConversations(), body)
      .then(success => {
        if (success) {
          this.updateConversations();
        }
        return success; // return the result (success/error/data)
      });
  }

  getReportReasons() {
    return this.serverService.sendGetRequest(urls.reportReasons.getReasons);
  }


  updateConversations() {
    this.checkCloseCurrentChat();
    // Hold the current page
    this.tempPage = this.cache.page;
    // Delete the cache conversations
    this.cache = {
      folder: this.cache.folder,
      page: 1,
      filter: this.cache.filter,
      conversations: []
    };
    // Get the updated conversations
    this.loadConversationsByFolder(this.cache.folder, this.cache.filter);
    this.tempPage--;
    while (this.cache.hasMore && this.tempPage > 0) {
      this.loadMore();
      this.tempPage--;
    }
    this.checkAll.next(false);
  }

  checkCloseCurrentChat() {
    // Close the current conversation if needed
    const index = this.selectedUsers.findIndex(userId => userId === this.convId);
    if (index !== -1) {
      this.closeCurrentConv.next();
    }
  }

  closeCurrentAfterSingleRemove(userId) {
    if (this.convId === userId) {
      this.closeCurrentConv.next();
    }
  }

  removeUser(userId) {
    const conv = _.find(this.cache.conversations, function (conversation) {
      return conversation.conversationUser.userId === userId ;
    });
    if (conv) {
      // Remove the wanted conversation
      this.cache.conversations.splice(this.cache.conversations.indexOf(conv), 1);
      this.closeCurrentAfterSingleRemove(userId);
    }
  }

  insertAllConversations() {
    this.selectedUsers = [];
    this.displayChatOptions.next(true);
    for (this.tempPage = 0; this.tempPage < this.cache.conversations.length; this.tempPage++) {
      this.insertUserToSelected(this.cache.conversations[this.tempPage].conversationUser.userId);
    }
  }

  removeUserFromSelected(id) {
    let index = this.selectedUsers.findIndex(userId => userId === id);
    while (index !== -1) {
      this.selectedUsers.splice(index, 1);
      index = this.selectedUsers.findIndex(userId => userId === id);
    }
    this.displayChatOptions.next(this.selectedUsers.length > 0);
  }

  removeAllConversation() {
    this.selectedUsers = [];
    this.displayChatOptions.next(false);
  }

  insertUserToSelected(id) {
    this.selectedUsers.push(id);
    this.displayChatOptions.next(true);
  }

  setRecentConversations(id) {
    // Find index
    const index = this.cache.conversations.findIndex(conversation => conversation.conversationUser.userId === id);
    if (index !== -1) {
      // Insert to beginning
      this.cache.conversations.unshift(this.cache.conversations[index]);
      // Remove from index
      this.cache.conversations.splice(index + 1, 1);
      this.allConversations.next(this.cache.conversations);
    } else {
      // // Get the conversation
      // const conv = this.loadConversationById(id);
      // if (conv) {
      //   this.cache.conversations.unshift(conv);
      // }
    }
  }

  pushNotification() {
    this.newMsgNotification.next(this.cache.conversations[0]);
  }

  decode() {
    let body = '';
    for (let i = 0; i < this.selectedUsers.length; i++) {
      body += 'userIds[]=' + this.selectedUsers[i] + '&';
    }
    body = body.substring(0, body.length - 1);
    return body;
  }

  updateConversation(msg, toUserId) {
    const conv = _.find(this.cache.conversations, function (conversation) {
      return conversation.conversationUser.userId === toUserId;
    });
    if (conv) {
      conv.lastMessageSent = msg.successfulySent;
      conv.lastMessageSentTime = msg.dateCreated;
      conv.lastMessageRead = msg.isRead;
      conv.lastMessageReadime = msg.dateRead;
      conv.lastMessageMy = msg.isViewingUserMessage;
    }
  }

  updateConversationOnSendMsg(msg, toUserId, pushNotification) {
    const conv = _.find(this.cache.conversations, function (conversation) {
      return conversation.conversationUser.userId === toUserId;
    });
    if (conv) {
      // Remove the wanted conversation
      this.cache.conversations.splice(this.cache.conversations.indexOf(conv), 1);
      // Insert the wanted conversation to the beginning
      this.cache.conversations.unshift(conv);
      if (pushNotification) {
        this.pushNotification();
        // Insert if the conversation isn't include in the current active
        if (_.findIndex(this.activeConversations.conversations, {conversationUser: {userId: toUserId}}) === -1) {
          this.activeConversations.unReadMessages++;
          this.pushNewActiveConversation(toUserId);
        } else if (this.activeConversations.prevCurrentId === toUserId && !this.activeConversations.counterOn) {
          this.activeConversations.unReadMessages++;
          this.activeConversations.counterOn = true;
          this.activeConvs.next(this.activeConversations.conversations);
        }
      }
      // Update the wanted conversation information
      conv.lastMessageSent = msg.successfulySent;
      conv.lastMessageSentTime = msg.dateCreated;
      conv.lastMessageMy = msg.isViewingUserMessage;
      conv.lastMessageRead = msg.isRead;
      conv.isRead = msg.isRead;
      conv.last_message_content = msg.content.substring(0, 38) +((msg.content.length > 38)?"...":"");
    } else {
      // Get the new conversation details in insert it at the beginning
      return this.serverService.sendGetRequest(urls.conversations.byId(toUserId))
        .then(data => {
          this.logger.debug('new message conversation user is ' , data.conversations);
          return this.convertConversation(data.conversations[0]);
        })
        .then(conversations => {
          // Insert if the conversation isn't include in the current active
          this.addConversation(conversations);
          this.pushNotification();
        });
    }
  }

  getConversationById(cacheConversations, userId) {
      const conv = _.find(cacheConversations, function (conversation) {
        return conversation.conversationUser.userId.toString() === userId;
      });
      if (conv) {
        this.setDefaultConversation.next(conv);
      } else {
        // Get the new conversation details in insert it at the beginning
        this.serverService.sendGetRequest(urls.conversations.byId(userId))
          .then(data => {
            this.logger.debug('new message conversation user is ' , data.conversations);
            if (data.conversations && data.conversations[0]) {
              return this.convertConversation(data.conversations[0]) ;
            } else {
              return this.createConversation(data.conversation_user);
            }
          })
          .then(conversations => {
            // Insert if the conversation isn't include in the current active
            const index = this.cache.conversations.findIndex
            (conversation => conversation.conversationUser.userId === conversations.conversationUser.userId);
            if (index === -1) {
              this.cache.conversations.unshift(conversations) ;
            }
            this.setDefaultConversation.next(conversations);
          });
    }

  }

  createConversation(conversationUser) {
    const conversation = new Conversation();
    conversation.isRead = true;
    conversation.logo = conversationUser.logo;
    conversation.name = conversationUser.nickname;
    conversation.lastMessageMy = false;
    conversation.lastMessageSentTime = null;
    conversation.lastMessageReadime = null;
    conversation.lastMessageRead = false;
    conversation.totalMessages = 0;
    conversation.lastMessageSent = false;
    conversation.last_message_content = this.translateService.format('NO_RECENT_MESSAGES');
    conversation.conversationUser = this.userService.convertUserResponse(conversationUser);
    conversation.conversationUser.nickName = conversationUser.nickname;
    conversation.conversationUser.totalNotes = conversationUser.total_notes;
    return conversation;
  }

  updateConversationOnReadMsg(toUserId) {
    const conv = _.find(this.cache.conversations, function (conversation) {
      return conversation.conversationUser.userId === toUserId;
    });
    if (conv) {
      conv.lastMessageReadime = moment().unix();
      conv.lastMessageRead = true;
    }
  }

  reportUser(userId, reasonId, reportText) {
    const body = reportText ? 'reasonId=' + reasonId + '&message=' + reportText :
      'reasonId=' + reasonId;
    return this.serverService.sendPostRequest(urls.post.report(userId), body)
      .then(response => {
        return response; // return the result (success/error/data)
      });
  }

  getLastMessageMyFromConv(userId) {
    const conv = _.find(this.cache.conversations, function (conversation) {
      return conversation.conversationUser.userId === userId;
    });
    if (conv) {
      return conv.lastMessageMy;
    }
    return true;
  }

  increaseTotalMessages(userId) {
    const conv = _.find(this.cache.conversations, function (conversation) {
      return conversation.conversationUser.userId === userId;
    });
    if (conv) {
      conv.totalMessages++;
    }
  }

  pushNewActiveConversation(userId) {
    const index = _.findIndex(this.cache.conversations, {conversationUser: {userId: userId}});
    if (index !== -1) {
      this.activeConversations.conversations.push(this.cache.conversations[index]);
      this.activeConvs.next(this.activeConversations.conversations);
    }
  }

  popConversationFromActive(userId, conversation) {
    const index = _.findIndex(this.activeConversations.conversations, {conversationUser: {userId: userId}});
    if (index !== -1) {
      if (!conversation.lastMessageMy && !conversation.lastMessageRead
        || (this.activeConversations.conversations[index].conversationUser.userId === this.activeConversations.prevCurrentId &&
          this.activeConversations.counterOn)) {
        this.activeConversations.unReadMessages--;
        this.activeConversations.counterOn = false;
      }
      console.log('removing conversation from active ' , this.activeConversations.conversations[index]);
      this.activeConversations.conversations.splice(index , 1);
      this.activeConvs.next(this.activeConversations.conversations);
    }
  }

  private addConversation(newConversation) {
    const index = this.cache.conversations.findIndex
    (conversation => conversation.conversationUser.userId === newConversation.conversationUser.userId);
    if (index !== -1) {
      this.cache.conversations[index] = newConversation;
    } else {
      this.cache.conversations.unshift(newConversation) ;
      this.activeConversations.unReadMessages++;
      this.pushNewActiveConversation(newConversation.conversationUser.userId);
    }
  }

  // Search for default conversation
  getDefaultConversation(conversations) {
    // console.log('in default conversation are' , this.cache.conversations);
    for (let i = 0; i < conversations.length; i++) {
      if (!conversations[i].lastMessageMy && conversations[i].isRead) {
        return conversations[i].conversationUser.userId;
      }
    }
    return conversations[0] ? conversations[0].conversationUser.userId : null;
  }

  public getUserProfileUrl(userId) {
    return 'https://xfinder3.com/?p=' + userId ;
  }

  // Set default conversation id from url
  setDefaultConversationId(userId) {
    this.defaultConversationId = userId;
    this.isConversationFromUrlParams = true;
  }

  getIsConversationFromUrlParams() {
    return this.isConversationFromUrlParams;
  }
}
