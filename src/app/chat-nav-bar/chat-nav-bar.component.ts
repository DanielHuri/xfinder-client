import {Component, TemplateRef, ViewChild} from '@angular/core';
import {ConversationService} from '../conversation/conversation.service';
import {Folders} from '../conversation/folders.enum';

@Component({
  selector: 'chat-nav-bar',
  templateUrl: './chat-nav-bar.component.html',
  styleUrls: ['./chat-nav-bar.component.scss']
})
export class ChatNavBarComponent {

  inReceived = true;
  filter = false;

  constructor(private conversationService: ConversationService) {
  }

  inbox() {
    this.inReceived = true;
    this.filter = false;
    this.conversationService.loadInboxConversations(Folders.FILTER_NONE);
  }

  outbox() {
    this.inReceived = false;
    this.filter = false;
    this.conversationService.loadOutboxConversations(Folders.FILTER_NONE);
  }
  unreadInbox() {
    this.conversationService.loadInboxConversations(Folders.FILTER_UNREAD);
  }

  unreadOutbox() {
    this.conversationService.loadOutboxConversations(Folders.FILTER_UNREAD);
  }

  changeRead() {
    this.filter = !this.filter;
    if (this.inReceived) {
      if (this.filter) {
        this.unreadInbox();
      } else {
        this.inbox();
      }
    } else {
      if (this.filter) {
        this.unreadOutbox();
      } else {
        this.outbox();
      }
    }
  }
}
