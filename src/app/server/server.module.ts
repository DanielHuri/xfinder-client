import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {ServerService} from './server.service';
import {WebsocketService} from '../websocket/websocket.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [],
  providers: [
    ServerService,
    WebsocketService
  ]
})
export class ServerModule {
}
