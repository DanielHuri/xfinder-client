import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LoggerService} from '../logger/logger.service';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../environments/environment';
import {MobileViewService} from '../mobile-view/mobile-view.service';

@Injectable()
export class ServerService {

  currentUid = '';

  constructor(private http: HttpClient,
              private logger: LoggerService,
              private activatedRoute: ActivatedRoute,
              private mobileService: MobileViewService) {
  }

  // Server

  sendGetRequest(url): Promise<any> {
    return this.createOptions()
      .then((options) => {
        // url = `${url}?uid=${this.currentUid}`;
        this.logger.verbose(`GET --> ${url}`);
        return this.http.get(url, options).toPromise();
      })
      .then(response => {
        this.logger.verbose(`GET - result from ${url}`, response);
        return response;
      })
      .then((response: any) => {
        if (!response.success) {
          throw new Error(response.error);
        }

        return response.data;
      })
      .catch(err => {
        this.logger.error(`GET - error from ${url}`, err);
        if (err && err.error && err.error.error && err.error.error.code && err.error.error.code === 'must_be_premium') {
          this.mobileService.needsPremiumEvent.next();
        }
        throw err;
      });
  }

  sendPostRequest(url, body): Promise<any> {
    return this.createOptions()
      .then((options) => {
        // url = `${url}?uid=${this.currentUid}`;
        const strBody = body.toString();
        this.logger.verbose(`POST --> ${url} with body`, strBody);
        return this.http.post(url, strBody, options).toPromise();
      })
      .then(response => {
        this.logger.verbose(`POST - result from ${url}`, response);
        return response;
      })
      .then((response: any) => {
        if (!response.success) {
          throw new Error(response.error);
        }

        return response.data;
      })
      .catch(err => {
        this.logger.error(`GET - error from ${url}`, err);
        if (err && err.error && err.error.error && err.error.error.code && err.error.error.code === 'must_be_premium') {
          this.mobileService.needsPremiumEvent.next();
        }
        throw err;
      });
  }

  sendPutRequest(url, body): Promise<any> {
    return this.createOptions()
      .then((options) => {
        // url = `${url}?uid=${this.currentUid}`;
        const strBody = body.toString();
        this.logger.verbose(`PUT --> ${url} with body`, strBody);
        return this.http.put(url, strBody, options).toPromise();
      })
      .then(response => {
        this.logger.verbose(`PUT - result from ${url}`, response);
        return response;
      })
      .then((response: any) => {
        return this.checkResponse(response);
      })
      .catch(err => {
        this.logger.error(`PUT - error from ${url}`, err);
        throw err;
      });
  }

  sendDeleteRequest(url, body): Promise<any> {
    return this.createDeleteOptions(body.toString())
      .then((options) => {
        // url = `${url}?uid=${this.currentUid}`;
        const strBody = body.toString();
        this.logger.verbose(`PUT --> ${url} with body`, strBody);
        return this.http.delete(url, options).toPromise();
      })
      .then(response => {
        this.logger.verbose(`PUT - result from ${url}`, response);
        return response;
      })
      .then((response: any) => {
        return this.checkResponse(response);
      })
      .catch(err => {
        this.logger.error(`PUT - error from ${url}`, err);
        throw err;
      });
  }

  private checkResponse(response: any) {
    if (!response.success) {
      throw new Error(response.error);
    }

    return response.data;
  }

  private createOptions() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded');

    const options = {
      headers: headers
    };

    if (environment.production) {
      return Promise.resolve(options);
    } else {
      return new Promise((resolve, reject) => {
        this.activatedRoute.queryParams
          .subscribe(params => {
            const uidParam = params['uid'];
            if (uidParam) {
              this.currentUid = uidParam;
              resolve(options);
            }
          });
      });
    }
  }

  createDeleteOptions(body) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded');

    const options = {
      headers: headers,
      body: body
    };
    if (environment.production) {
      return Promise.resolve(options);
    } else {
      return new Promise((resolve, reject) => {
        this.activatedRoute.queryParams
          .subscribe(params => {
            const uidParam = params['uid'];
            if (uidParam) {
              this.currentUid = uidParam;
              resolve(options);
            }
          });
      });
    }
  }

  private createOptionsFormData() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'multipart/form-data; boundary=boundary');
    // .set('Content-Transfer-Encoding', 'binary');
    const options = {
      headers: headers,
      processData: false,
    };

    if (environment.production) {
      return Promise.resolve(options);
    } else {
      return new Promise((resolve, reject) => {
        this.activatedRoute.queryParams
          .subscribe(params => {
            const uidParam = params['uid'];
            if (uidParam) {
              this.currentUid = uidParam;
              resolve(options);
            }
          });
      });
    }
  }

  sendPostRequestFormData(url, body): Promise<any> {
    return this.createOptionsFormData()
      .then((options) => {
        // url = `${url}?uid=${this.currentUid}`;
        // const strBody = body.toString();
        // setTimeout(() => {
        //   this.logger.debug('upload failed');
        //   return {'success': false, 'data': {'images': [{'index': -1}]}};
        // }, 60000);
        return this.http.post(url, body, {})
          .timeout(30000).toPromise();
        // return this.http.post(url, strBody, options).toPromise();
      });
    // .catch(err => {
    //   this.logger.error(`POST - error from ${url}`, err);
    //   return 'timeout';
    // });
    // .then(response => {
    //   this.logger.verbose(`POST - result from ${url}`, response);
    //   return response;
    // })
    // .then((response: any) => {
    //   if (!response.success) {
    //     throw new Error(response.error);
    //   }
    //
    //   return response.data;
    // })
  }

}

export {urls} from './server-urls';
