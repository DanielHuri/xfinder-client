import {environment} from '../../environments/environment';

const baseUrl = environment.serverUrl;
// const baseJsonUrl = `${baseUrl}/json`;
const baseJsonUrl = `${baseUrl}`;
const baseMailBoxUrl = `${baseJsonUrl}/mailbox`;
const conversationUrl = `${baseMailBoxUrl}/conversation`;
const conversationsUrl = `${baseMailBoxUrl}/conversations`;
const baseBlockListUrl = `${baseJsonUrl}/blacklist`;

export const urls = {
  conversations: {
    byFolder: (folder, page, filter) => `${conversationsUrl}/${folder}/${page}/${filter}`,
    byId: (userId) => `${baseMailBoxUrl}/conversationDetails/${userId}`,
    // byFolder: (folder, page, filter) => `${conversationsUrl}/${folder}/${page}`,
  },
  conversation: {
    byUserId: (userId, page) => `${conversationUrl}/${userId}/${page}`,
  },
  currentUser: {
    getCurrent: `${baseJsonUrl}/users/me`
  },
  reportReasons: {
    getReasons: `${baseJsonUrl}/spam/getReasons`
  },
  gallery: {
    getImages: `${baseMailBoxUrl}/gallery`
  },
  post: {
    saveMessage: (userId) => `${baseMailBoxUrl}/saveMessage/${userId}`,
    report: (userId) => `${baseJsonUrl}/spam/report/${userId}`,
    uploadImages: () => `${baseMailBoxUrl}/upload`,
  },
  put: {
    setConversationRead: (userId) => `${baseMailBoxUrl}/setConversationRead/${userId}`,
    updateBlockList: (action) => `${baseBlockListUrl}/${action}`,
    moveConversations: () => `${baseMailBoxUrl}/moveConversations`,
  },
  delete: {
    deleteImage: () => `${baseMailBoxUrl}/deleteImages`,
  }
};
