import {ConversationService} from './../conversation/conversation.service';
import {MobileViewService} from './../mobile-view/mobile-view.service';
import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {Observable, pipe, Subject} from 'rxjs';
import {LoggerService} from '../logger/logger.service';
import {MessageService} from '../message/message.service';
import {UserService} from '../user/user.service';
import {Conversation} from '../classes/conversation';
import {GalleryService} from '../gallery.service';
import {Message} from '../classes/message';
import {User} from '../classes/user';
import {fromEvent} from 'rxjs/observable/fromEvent';
import * as Rx from 'rxjs';
import * as moment from 'moment';
import {TranslateService} from '../translate.service';

declare const $: any;

@Component({
  selector: 'chat-window',
  templateUrl: './chat-window.component.html',
  styleUrls: ['./chat-window.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatWindowComponent implements AfterViewInit {

  messages: Observable<any>;
  @ViewChild('chatWindow') messageElement: any;
  @ViewChild('textTyping') typingMsg: any;
  @ViewChild('paymentWindow') paymentWindow: any;
  @ViewChild('profileLinkBtn') profileLinkBtn: any;
  @Input() currentConversation: Conversation;
  lastConversations;
  currentUser: User;
  isDesktop: boolean;
  isMobile:  boolean;
  isClientSide:boolean;
  isFloatRight: boolean;
  shouldDisplayHeader: boolean;
  isInApplicationIframe: boolean;
  draftMessage: Message = new Message();
  typing;
  showActiveChat = false;
  showDisabled = true;
  myPlaceHolder = 'כתוב הודעה';
  inZoomImg = false;
  amountActiveNotRead;
  hideWindowHeader = false;
  allMessagesLoaded = false;
  getUserProfileUrl_Reference;

  constructor(private logger: LoggerService,
              private messageService: MessageService,
              private userService: UserService,
              private conversationsService: ConversationService,
              private mobileViewService: MobileViewService,
              private el: ElementRef,
              private  galleryService: GalleryService,
              private ref: ChangeDetectorRef,
              private translateService: TranslateService) {
    this.isDesktop = $(window).width() > 700;
    this.isMobile = this.detectmob();
    this.isFloatRight = this.translateService.isFloatRight();
    this.getUserProfileUrl_Reference = this.conversationsService.getUserProfileUrl; // Html cant access function from service
    this.isInApplicationIframe = this.userService.isIframeInApplication();
    this.shouldDisplayHeader = this.isInApplicationIframe || this.isDesktop;
    this.messages = this.messageService.currentConversationsMessages;

    this.conversationsService.currentConversation.subscribe(() => {
      this.draftMessage.content = '';
      this.showDisabled         = true;
      this.allMessagesLoaded    = false;
    });

    this.userService.currentUser
      .subscribe(user => {
        this.currentUser = user;
      });
    // the local variable typing will be updated async when isTyping get updated automatic "next"
    this.typing = this.messageService.isTyping;
    this.lastConversations = this.conversationsService.activeConvs;
    this.conversationsService.activeConvs.subscribe( () => {
      this.amountActiveNotRead = this.conversationsService.activeConversations.unReadMessages;
      setTimeout(() => {
        this.ref.detectChanges();
      });
    });
    this.isClientSide = !this.isNotClient();
  }


  ngAfterViewInit() {
    this.messages
      .filter(() => {
        return (this.messageElement.nativeElement.scrollTop + this.messageElement.nativeElement.offsetHeight
          + 5 > this.messageElement.nativeElement.scrollHeight);
      })
      .subscribe(messages => {
        this.logger.debug('messages from window', messages);
        setTimeout(() => {
          this.scrollToBottom();
        });
      });

    Rx.Observable
      .fromEvent(this.messageElement.nativeElement, 'scroll')
      .filter(() => {
        return (this.messageElement.nativeElement.scrollTop + this.messageElement.nativeElement.offsetHeight <
          this.messageElement.nativeElement.scrollHeight - 80) !== this.hideWindowHeader;
      })
      .distinctUntilChanged()
      .debounceTime(50)
      .filter( () => {
        // this.hideWindowHeader = !this.hideWindowHeader; // Display/Hide chat window header
        // this.ref.detectChanges();                       // Detect changes
        // if (!this.hideWindowHeader && this.messageElement.nativeElement.scrollTop + 38 + this.messageElement.nativeElement.offsetHeight <
        //   this.messageElement.nativeElement.scrollHeight - 80 === false) {
        //   setTimeout(() => {
        //     this.scrollToBottom();
        //   });
        // }
        return !this.allMessagesLoaded;
      })
      .distinctUntilChanged()
      .subscribe( () => {
        this.allMessagesLoaded = true;
        this.messageService.loadAll(this.currentConversation.conversationUser.userId);
      });

      this.mobileViewService.needsPremiumEvent
        .subscribe( () => {
          this.currentUser.needsPremium = true;
          this.paymentWindow.nativeElement.click();
        });
  }

  onBlurTextArea(event: any): void {
    this.postMessageToParent('onBlur');
  }

  onFocusTextArea(event: any): void {
    this.postMessageToParent('onFocus');
  }

  postMessageToParent(event: string) {
    window.parent.postMessage(event, "*");
  }

  onSendClick(event: any): void {
    this.sendMessage();
    event.preventDefault();
  }

  sendMessage(): void {
    this.scrollToBottom();
    if (this.draftMessage.content) {
      this.typingMsg.nativeElement.style.height = 'unset';
      this.draftMessage.content = this.draftMessage.content.replace(/\n/g, '<br />');
      const message: Message = this.draftMessage;
      message.tempId = Date.now().toString();
      message.dateCreated = moment().unix();
      message.isRead = false;
      message.isViewingUserMessage = true;
      message.successfulySent = false;
      this.messageService.sendMessage(this.currentConversation.conversationUser.userId, message);
      this.draftMessage = new Message();
    }

    this.showDisabled = true;
  }

  sendImage(image): void {
    this.draftMessage.image = image;
    this.draftMessage.content = ' ';
    this.sendMessage();
  }

  scrollToBottom() {
    this.messageElement.nativeElement.scrollTop = this.messageElement.nativeElement.scrollHeight;
  }

  toggleActiveChat() {
    this.showActiveChat = !this.showActiveChat;
  }

  changeDisabled() {
    // check if it has content and the content isn't only '\n' or ' '
    if (this.draftMessage.content === '\n' || this.draftMessage.content === ' ' || this.draftMessage.content === '') {
      this.draftMessage.content = '';
      this.showDisabled = true;
      this.typingMsg.nativeElement.style.height = 'unset';
    }
    if (!this.showDisabled && this.draftMessage.content.includes('\n') && (this.isDesktop || this.isMobile)) {
      this.draftMessage.content = this.draftMessage.content.trim();
      this.sendMessage();
    }
  }

  sendTyping() {
    console.log(`sendTyping`);

    Rx.Observable
      .of (true)  // Create observable with value -> true
      .debounceTime(300)
      .subscribe((val) => {
        this.messageService.setTyping(this.currentConversation.conversationUser.userId, val);
      });
  }

  setPlaceHolder() {
    this.showDisabled = false;
    if ((this.isMobile||this.isDesktop)&& this.draftMessage.content.includes('\n') ) {

    } else {
      this.typingMsg.nativeElement.style.height = '1px' ;
      this.typingMsg.nativeElement.style.height = (this.typingMsg.nativeElement.scrollHeight) + 'px' ;
    }
  }

  goBack() {
    if (this.conversationsService.getIsConversationFromUrlParams()) {
      // Return to user's profile
      window.location.href = this.conversationsService.getUserProfileUrl(this.currentConversation.conversationUser.userId);
    } else {
      // Return to conversations lobby
      this.mobileViewService.setIsOnConversation(true);
    }
  }

  zoomOut() {
    this.messageElement.nativeElement.style.content = '';
    this.messageElement.nativeElement.style.height = '';
    this.messageElement.nativeElement.style.width = '100%';
    this.messageElement.nativeElement.style.marginBottom = '48px';
    this.messageElement.nativeElement.style.maxHeight = 'unset';
    this.messageElement.nativeElement.style.bottom = '-4px';

    this.inZoomImg = false;
  }

  changeConversation(conversation) {
    this.conversationsService.setCurrentConversation(conversation);
    this.messageService.updateSocketIoConvRead(conversation.conversationUser.userId);
    this.showActiveChat = false;
  }

  getGallery() {
    this.galleryService.allImages.next();
  }

  openImage(url) {
    this.hideWindowHeader = false;
    this.inZoomImg = true;
    this.messageElement.nativeElement.style.content = 'url(' + url + ')' ;
    // this.messageElement.nativeElement.style.maxHeight = '80%';
    this.messageElement.nativeElement.style.height = '80%';
    this.messageElement.nativeElement.style.width = 'auto';
    this.messageElement.nativeElement.style.maxWidth = '100%';
    this.messageElement.nativeElement.style.margin = 'auto';
    this.ref.detectChanges();
  }

  next5Convs() {
    // this.lastConversations = this.
  }

  linkToProfile() {
    if (!this.isInApplicationIframe) {
      this.profileLinkBtn.nativeElement.click();
    }
  }

  detectmob() { 
    return this.mobileViewService.isMobileDeviceByNavigator();
  }

  isNotClient(){
    let count=0;
    let lastClient = 0;
    this.conversationsService.allConversations.subscribe(conversations => {
      conversations.forEach(conv =>{
        if(conv.user_one!== lastClient) {
         count++;
         lastClient = conv.user_one; 
        }
        });
      });
    return count>1;
  }

}