import { Injectable } from '@angular/core';
import {Languages} from './languages.enum';
import * as he from '../assets/i18n/he.json';
import * as en from '../assets/i18n/en.json';
import * as ru from '../assets/i18n/ru.json';
import * as ro from '../assets/i18n/ro.json';
import * as ch from '../assets/i18n/ch.json';

@Injectable()
export class TranslateService {
  data: any = {};
  currentLang: Languages;

  constructor() {
    this.currentLang = Languages.ENGLISH;
  }


  setLanguage(lang: Languages) {
    this.currentLang = lang;
    this.data = Object.assign({}, this.getJsonObject(lang) || {});
  }

  getJsonObject(lang: Languages) {
    switch (lang) {
      case Languages.HEBREW:
        return he;
      case Languages.ENGLISH:
        return en;
      case Languages.RUSSIAN:
        return ru;
      case Languages.ROMAN:
        return ro;
      case Languages.CHINA:
        return ch;
    }
  }

  format(key: string): string {
    return this.data[key];
  }

  getCurrentLang(): Languages {
    return this.currentLang;
  }

  isFloatRight() {
    return this.currentLang === Languages.HEBREW;
  }








}
