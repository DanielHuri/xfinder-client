import {BaseResponse} from './base-response';
import {Conversation} from './conversation';

export class FolderResponse extends BaseResponse {
  data: {
    conversations: Conversation[];
    hasMore: boolean;
    maxPage: number;
  };
}
