import {User} from './user';
import {Image} from '../image';

export class Message {
  messageId: string;
  userId: string;
  user: User;
  content: string;
  dateCreated: number;
  dateRead: number;
  isViewingUserMessage: boolean;
  tempId: string;
  isRead: boolean;
  successfulySent?: boolean;
  image?: Image;
  prevMsgDiffUser: boolean;
}
