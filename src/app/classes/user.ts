import {Languages} from '../languages.enum';

export class User {
  userId: string;
  online: boolean;
  nickName: string;
  lookingUserType: string;
  type: string;
  subtype: string;
  logo: string;
  needsPremium: boolean;
  age: number;
  city: string;
  totalNotes: number;
  country: string;
  lang: Languages;
  isWolf:boolean;
  phoneNumber:string;
}
