import {BaseResponse} from './base-response';
import {User} from './user';
import {Message} from './message';

export class UserConversationResponse extends BaseResponse {
  data: {
    totalMessages: number;
    viewingUser: User;
    conversationUser: User;
    messages: Message[];
    hasMore: boolean;
    maxPage: number;
  };
}
