import {User} from 'app/classes/user';
import { NumericDictionary } from 'lodash';

export class Conversation {
  name: string;
  user_one:number;
  user_two:number;
  conversationUser: User;
  totalMessages: number;
  isRead: boolean;
  lastMessageSentTime: number;
  lastMessageReadime: number;
  lastMessageRead: boolean;
  lastMessageMy: boolean;
  lastMessageSent: boolean;
  last_message_content?: string;
  is_wolf : boolean;
  logo: string;
}
