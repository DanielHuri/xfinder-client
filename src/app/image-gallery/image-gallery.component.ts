import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {GalleryService} from '../gallery.service';
import {ChatWindowComponent} from '../chat-window/chat-window.component';
import {Image} from '../image';
import {TranslateService} from '../translate.service';
import * as _ from 'lodash';


@Component({
  selector: 'app-image-gallery',
  templateUrl: './image-gallery.component.html',
  styleUrls: ['./image-gallery.component.scss']
})
export class ImageGalleryComponent implements OnInit {
  @ViewChild('closeGallery') closeGallery: any;
  @ViewChild('galleryImages') galleryImages: any;
  @ViewChild('inputFile') InputFile: any;
  @ViewChild('bodyElement') BodyElement: any;
  images: Image[];
  selectedImage: Image;
  selectedImages:Image[];
  isLoading = false;
  loadingURL = 'https://media.giphy.com/media/eeixlOgztK6F4937d0/giphy.gif';
  displayDeleteWarning = false;
  isFloatRight: boolean;

  constructor(private galleryService: GalleryService,
              private  chatWindowComp: ChatWindowComponent,
              private ref: ChangeDetectorRef,
              private translateService: TranslateService) {

    // this.images = this.galleryService.allImages;
    this.galleryService.allImages.subscribe(() => {
      if (!this.images.length) {
        this.isLoading = true;
        this.getGallery();
      }
    });

    this.galleryService.selectedImg
      .subscribe(img => {
        this.selectedImage = img;
        if(img.id>0){this.addImage();}
        else{this.removeImage();}
        this.ref.detectChanges();
      });

    this.galleryService.selectedImgs.subscribe(imgs=>{
      this.selectedImages = imgs;
      this.ref.detectChanges();
    });
  }

  ngOnInit(): void {
    this.images = [];
    this.selectedImage = null;
    this.selectedImages=[];
    this.galleryService.selectedImgs.next(this.selectedImages);
    this.isFloatRight = this.translateService.isFloatRight();
  }

  getImage() {
    this.InputFile.nativeElement.click(); // click on the "close" button
  }

  fileChangeEvent() {
    for (let i = 0; i < this.InputFile.nativeElement.files.length; i++) {
      this.readImageURL(i);
    }

    this.galleryService.uploadImage(this.InputFile.nativeElement.files)
      .then(response => {
        console.log('response from upload is ', response);
        return response;
      })
      .then((response: any) => {
        return response.data;
      })
      .then(data => {
        this.insertNewImage(data.images);
      })
      .catch(err => {
        console.log('error uploading ', err);
        this.failUploading();
      });
  }


  readImageURL(number) {
    const reader = new FileReader();
    reader.onloadend = (function (myThis) {
      return function (e) {
        myThis.pushNewImage(reader.result, number);
      };
    })(this);
    reader.readAsDataURL(this.InputFile.nativeElement.files[number]);
  }

  public getGallery() {
    this.galleryService.getGalleryImages()
      .then(data => {
        this.setImages(data.images);
      });
  }

  setImages(response) {
    // Code Section
    const emptyImage = [null, null, null, null, null, null, null, null, null]; // Default placeholders images
    // Merge gallery images with placeholders images
    this.images = response.reverse().concat(emptyImage);
    this.isLoading = false;
    this.ref.detectChanges();
  }

  insertNewImage(response) {
    const responseLength = response.length;
    for (let i = 0; i < responseLength; i++) {
      if (response[i].index) {
        // fail uploading current image
        this.galleryService.imageStatus.next({image: response[i], status: 'fail', tempId: i * -1});
      } else {
        // update the img object about the new properties
        const lastImage = i === responseLength - 1;
        const successData = {
          image: response[i],
          status: 'success',
          tempId: i * -1,
          lastImage: lastImage
        };
        this.galleryService.imageStatus.next(successData);
        // update the local array about the new id
        this.images[this.images.findIndex(img => img && img.id === i * -1)] = response[i];
      }
    }
  }

  failUploading() {
    for (let i = 0; i < this.InputFile.nativeElement.files.length; i++) {
      this.galleryService.imageStatus.next({image: {}, status: 'fail', tempId: i * -1});
    }
  }

  pushNewImage(url, index) {
    // Create Image with the source url
    const temp = new Image();
    temp.url = url;
    temp.id = index * -1;
    temp.height = 60;
    temp.width = 60;
    //  Push the new image
    this.images.unshift(temp);
    this.ref.detectChanges();
    // Update the image status
    const temp2 = new Image();
    temp2.id = temp.id;
    temp2.url = this.loadingURL;
    this.galleryService.imageStatus.next({image: temp2, status: 'loading', tempId: temp.id});
  }

  sendImage() {
    this.closeGallery.nativeElement.click(); // click on the "close" button
    for(let i=0; i<this.selectedImages.length;i++){
      this.chatWindowComp.sendImage(this.selectedImages[i]);
    }
    this.clearSelectedImages();
  }

  preformDelete() {
    for(let i=0; i<this.selectedImages.length;i++){
      const imageToDelete = this.selectedImages[i];
      this.galleryService.deleteImage(imageToDelete.id).then(() =>{this.deleteImageFromGallery(imageToDelete);});
    }
    this.clearSelectedImages();

    // if (this.selectedImage) {
    //   const imageToDelete = this.selectedImage;
    //   this.galleryService.deleteImage(imageToDelete.id)
    //     .then(() => {
    //       this.deleteImageFromGallery(imageToDelete);
    //     });
    //}
  }

  clearSelectedImages(){
    this.selectedImages = [];
    this.galleryService.selectedImgs.next(this.selectedImages);
    this.ref.detectChanges();
  }

  deleteImageFromGallery(image) {
    // Remove the image from the gallery
    this.images.splice(this.images.indexOf(image), 1);
    // Update the html images array
    this.ref.detectChanges();
  }

  changeGallery() {
    this.closeGallery.nativeElement.click(); // click on the "close" button
    // if (this.displayDeleteWarning) {
    //   this.displayDeleteWarning = false;
    // } else {
    //   this.closeGallery.nativeElement.click(); // click on the "close" button
    // }
  }

  addImage(){
    if(this.selectedImages.length<7){
      this.selectedImages.push(this.selectedImage);
      this.galleryService.selectedImgs.next(this.selectedImages);
      this.ref.detectChanges();
    }
  }

  removeImage(){
    this.selectedImages.splice(this.selectedImages.findIndex(img=> img.id === (this.selectedImage.id*-1)),1);
    this.galleryService.selectedImgs.next(this.selectedImages);
    this.ref.detectChanges();
  }
}


