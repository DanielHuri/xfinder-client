import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs/Rx';
import {Image} from './image';
import {ServerService, urls} from './server/server.service';
import * as _ from 'lodash';


@Injectable()
export class GalleryService {
  private gallery: {
    total_images: number ,
    images: Image[];
  };

  // create All images
  allImages: Subject<any> = new Subject();
  // Current image
  selectedImg: Subject<Image> = new Subject();
  selectedImgs: Subject<Image[]> = new Subject();
  // changeStatus
  imageStatus: Subject<any> = new Subject();
  constructor(private serverService: ServerService) { }

  loadGallery() {

  }

  uploadImage(images) {
    const body = new FormData();
    for ( let i = 0; i < images.length; i++) {
      body.append('images[]', images[i]);
    }
    return this.serverService.sendPostRequestFormData(urls.post.uploadImages(), body);
  }

  getGalleryImages() {
    return this.serverService.sendGetRequest(urls.gallery.getImages);
  }

  deleteImage(imageId) {
    const body = new URLSearchParams();
    body.set('imageIds[]', imageId);
    return this.serverService.sendDeleteRequest(urls.delete.deleteImage(), body);
  }

}
