export const events = {
  uid: 'uid',
  newMessage: 'new-message',
  typing: 'typing',
  convRead: 'convRead',
  logEvent: 'logEvent',
  getUid: 'getUid'
};
