import {Injectable} from '@angular/core';
import {LoggerService} from '../logger/logger.service';
import * as io from 'socket.io-client';
import {environment} from '../../environments/environment';
import Socket = SocketIOClient.Socket;
import {Observable, Subject} from 'rxjs';
import {events} from './events';
import {ReplaySubject} from 'rxjs/ReplaySubject';

@Injectable()
export class WebsocketService {

  // Our socket connection
  socket: Socket;
  socketSubject: Subject<any>;
  replaySubject: ReplaySubject<any>; // Used to catch udi requests from server

  constructor(private logger: LoggerService) {
    this.connect();
  }



  connectWithUid(id) {
    this.socketSubject.next({event: events.uid, args: {id}});
  }

  connect() {
    this.socket = io(environment.ws_url);

    // We define our observable which will observe any incoming messages
    // from our socket.io websocket.
    const observable = new Observable(observer => {

      this.socket.on(events.getUid, (data) => {
        this.logger.debug('on getUid request in websocket service');
        observer.next({event: events.getUid, data});
      });

      this.socket.on(events.newMessage, (data) => {
        this.logger.debug('on message in websocket service');
        observer.next({event: events.newMessage, data});
      });
      // listen to new typing event from the socket io
      this.socket.on(events.typing, (data) => {
        this.logger.debug('on typing in websocket service 222222');
        //  Pass typing event to message.service
        observer.next({event: events.typing, data});
      });

      // listen conv read event from the socket io
      this.socket.on(events.convRead, (data) => {
        this.logger.debug('on conversation read in websocket service 222222');
        observer.next({event: events.convRead, data});
      });

      return () => {
        this.logger.debug('disconnect from socket');
        this.socket.disconnect();
        // observer.complete();
      };
    });

    const observer = {
      next: (data) => {
        const {event, args} = data;
        this.logger.verbose(`emit event: '${event}' with args:`, args);
        this.socket.emit(event, args);
      }
    };

    this.socketSubject = Subject.create(observer, observable);

    this.replaySubject = new ReplaySubject<any>();
    this.socketSubject.subscribe(data => this.replaySubject.next(data));

    return this.socketSubject;
  }

  getSocket() {
    return this.socketSubject;
  }

  getReplaySocket() {
    return this.replaySubject;
  }
}

export {events as webSocketEvents} from './events';
