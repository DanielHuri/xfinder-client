import {Component, ElementRef, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {UserService} from './user/user.service';
import {ConversationService} from './conversation/conversation.service';
import {MessageService} from './message/message.service';
import {ChatConversationsComponent} from './chat-conversations/chat-conversations.component';
import {MobileViewService} from './mobile-view/mobile-view.service';
import {NotificationsService} from 'angular2-notifications';
import {LogEventHandlerService} from './log-event-handler/log-event-handler.service';
import {TranslateService} from './translate.service';
import {Languages} from './languages.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild('chatScroll') messageElement: any;
  @ViewChild(ChatConversationsComponent) chatConComp: ChatConversationsComponent;
  @ViewChild('notificate') example: TemplateRef<any>;
  currentConversation;
  convsVisible;
  chatVisible;
  notificationUser;
  notificationMsg;
  hideChat = false;
  loadingConversations = false;
  headerFloatRight: boolean;
  showDefaultMessage;
  public options = {
    position: ['top', 'left'],
    timeOut: 4500,
    preventLastDuplicates: true,
    animate: 'fromTop',
    showProgressBar: false,
    pauseOnHover: false,
    clickToClose: true,
    theClass: 'myClass'
  };

  constructor(private usersService: UserService,
              private messageService: MessageService,
              private conversationService: ConversationService,
              private mobileService: MobileViewService,
              private notification: NotificationsService,
              private errorEventHandler: LogEventHandlerService,
              private translateService: TranslateService,
              private elementRef: ElementRef) {
    this.readUserLanguage();

    this.conversationService.currentConversation
      .subscribe(conversation => {
        this.currentConversation = conversation;
        this.messageService.typingOffNewConversation(this.currentConversation.conversationUser.userId);
        this.hideChat = false;
      });

    this.conversationService.closeCurrentConv
      .subscribe(() => {
        this.hideChat = true;
        this.messageService.currentConversationClosed();
      });

    this.mobileService.isOnConv
      .subscribe(showingConvs => {
        this.convsVisible = showingConvs;
        this.chatVisible = !this.convsVisible;
        if (!this.chatVisible) {
          this.messageService.currentConversationClosed();
        }
      });

    this.conversationService.newMsgNotification
      .subscribe(newMsgConv => {
        this.notificationUser = newMsgConv;
        this.notificationMsg = translateService.format('NEW_MESSAGE_RECEIVED');
        this.displayNotification();
      });

    this.conversationService.setDefaultConversation
      .subscribe(conversation => {
        // this.conversationService.setCurrentConversation(conversation);
        // setTimeout(() => {
        //   this.conversationService.setCurrentConversation(conversation);
        // });
        this.conversationService.setCurrentConversation(conversation);
        this.messageService.updateSocketIoConvRead(conversation.conversationUser.userId);
        this.mobileService.setIsOnConversation(false);
      });

    this.showDefaultMessage = this.conversationService.pushSystemMessage;
  }

  ngOnInit(): void {
    // Init data of the current user
    // Also connect to socket.io
    this.usersService.initCurrentUser()
      .then(() => {
        this.logBrowserNavigatorObject();
      });
  }

  readUserLanguage() {
    let lang = new URLSearchParams(window.location.search).get('lang');
    if (!lang) {
      lang = Languages.HEBREW; // Default lang
    }
    this.translateService.setLanguage(lang);
    this.headerFloatRight = this.translateService.isFloatRight();
    this.setCssAccordingToLang();
  }

  logBrowserNavigatorObject() {
    const navigatorObj: any = window.navigator;
    const appCodeName = navigatorObj.appCodeName;
    const appName = navigatorObj.appName;
    const appVersion = navigatorObj.appVersion;
    const cookieEnabled = navigatorObj.cookieEnabled;
    const userAgent = navigatorObj.userAgent;
    const vendor = navigatorObj.vendor;
    const objToLog = {
      appCodeName,
      appName,
      appVersion,
      cookieEnabled,
      userAgent,
      vendor
    };
    this.errorEventHandler.logInfo(objToLog);
  }

  onScroll() {
    // scrollTop - amount scrolled
    // viewHeight - the base element height according to the screen size
    // scrollHeight - the total scrolling distance
    if (this.messageElement.nativeElement.scrollTop + this.messageElement.nativeElement.offsetHeight >
      65 * this.messageElement.nativeElement.scrollHeight / 100) {
      this.chatConComp.loadMore();
    }
  }

  displayNotification() {
    this.notification.bare(this.example);
  }

  setCurrentConversation() {
    this.conversationService.setCurrentConversation(this.notificationUser);
    this.messageService.updateSocketIoConvRead(this.currentConversation.conversationUser.userId);
    this.mobileService.setIsOnConversation(false);
  }

  setCssAccordingToLang() {
    if (!this.translateService.isFloatRight()) {
      this.elementRef.nativeElement.style.setProperty('--main-float', 'left');      // Set float var
      this.elementRef.nativeElement.style.setProperty('--not-main-float', 'right');      // Set float var
      this.elementRef.nativeElement.style.setProperty('--text-align', 'left');  // Set text direction var
      this.elementRef.nativeElement.style.setProperty('--text-direction', 'ltr');   // Set direction var
      this.elementRef.nativeElement.style.setProperty('--not-main-text-direction', 'rtl');   // Set direction var
      this.elementRef.nativeElement.style.setProperty('--arrow-icons-transform', 'unset');   // Set arrow icon direction
    }

    if (this.translateService.getCurrentLang() === Languages.RUSSIAN) {
      this.elementRef.nativeElement.style.fontSize = '0.9em';
      this.elementRef.nativeElement.style.setProperty('--block-conversation-font-size', '0.95em');
      this.elementRef.nativeElement.style.setProperty('--block-conversation-margin-top', '1.5px');
    }

    const textLength = this.translateService.format('READ_MORE').length;
    if (textLength <= 9) {
      // DO nothing
    } else if (textLength <= 13) {
      this.elementRef.nativeElement.style.setProperty('--read-more-short-number-font-size', '0.83em');
      this.elementRef.nativeElement.style.setProperty('--read-more-long-number-font-size', '0.75em');
    } else {
      this.elementRef.nativeElement.style.setProperty('--conversation-read-more-text-max-width', '55px');
      this.elementRef.nativeElement.style.setProperty('--read-more-short-number-font-size', '0.83em');
      this.elementRef.nativeElement.style.setProperty('--read-more-long-number-font-size', '0.77em');
    }

    const writeNoteLength = this.translateService.format('WRITE_NOTE').length;
    if (writeNoteLength >= 16) {
      this.elementRef.nativeElement.style.setProperty('--conversation-write-note-top', '-7px');
    }

  }
}
