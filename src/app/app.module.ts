import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {SimpleNotificationsModule} from 'angular2-notifications';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {UserService} from './user/user.service';
import {ThreadsService} from './thread/threads.service';
import {MessagesService} from './message/messages.service';

import {AppComponent} from './app.component';
import {ChatMessageComponent} from './chat-message/chat-message.component';
import {ChatConversationComponent} from './chat-conversation/chat-conversation.component';
import {ChatNavBarComponent} from './chat-nav-bar/chat-nav-bar.component';
import {ChatConversationsComponent} from './chat-conversations/chat-conversations.component';
import {ChatWindowComponent} from './chat-window/chat-window.component';
import {FromNowPipe} from './pipes/from-now.pipe';
import {LoggerModule} from './logger/logger.module';
import {ServerModule} from './server/server.module';
import {ConversationService} from './conversation/conversation.service';
import {MessageService} from './message/message.service';
import {MobileViewService} from './mobile-view/mobile-view.service';
import {ChatOptionsComponent} from './chat-options/chat-options.component';
import {MessageDatePipe} from './message-date.pipe';
import {ImageGalleryComponent} from './image-gallery/image-gallery.component';
import {GalleryService} from './gallery.service';
import {LogEventHandlerService} from './log-event-handler/log-event-handler.service';
import {ImageComponent} from './image/image.component';
import { TranslateService } from './translate.service';
import { TranslatePipe } from './translate.pipe';
import { ChatSystemMessageComponent } from './chat-system-message/chat-system-message.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    ChatMessageComponent,
    ChatConversationComponent,
    ChatNavBarComponent,
    ChatConversationsComponent,
    ChatWindowComponent,
    ChatOptionsComponent,
    FromNowPipe,
    MessageDatePipe,
    ImageGalleryComponent,
    ImageComponent,
    TranslatePipe,
    ChatSystemMessageComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes, {}),
    LoggerModule,
    ServerModule,
    BrowserAnimationsModule,
    SimpleNotificationsModule.forRoot(),
  ],
  providers: [
    MessagesService,
    ThreadsService,
    UserService,
    ConversationService,
    MessageService,
    MobileViewService,
    GalleryService,
    LogEventHandlerService,
    TranslateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
