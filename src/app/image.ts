
export class Image {
  id: number;
  url: string;
  height: number;
  width: number;
}
