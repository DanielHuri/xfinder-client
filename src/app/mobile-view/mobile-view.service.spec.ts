import { TestBed, inject } from '@angular/core/testing';

import { MobileViewService } from './mobile-view.service';

describe('MobileViewService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MobileViewService]
    });
  });

  it('should be created', inject([MobileViewService], (service: MobileViewService) => {
    expect(service).toBeTruthy();
  }));
});
