import { Injectable } from "@angular/core";
import { Subject, BehaviorSubject } from "rxjs";
import { ServerService, urls } from "../server/server.service";
import { LoggerService } from "../logger/logger.service";

@Injectable()
export class MobileViewService {
  private cache = {
    isOnConv: false
  };
  public needsPremiumEvent: Subject<any> = new Subject();
  public isOnConv: Subject<any>;

  constructor() {
    this.isOnConv = new BehaviorSubject(this.cache);
  }

  setIsOnConversation(val) {
    this.cache.isOnConv = val;
    this.isOnConv.next(val);
  }

  isMobileDeviceByNavigator() {
    if (
      navigator.userAgent.match(/Android/i) ||
      navigator.userAgent.match(/webOS/i) ||
      navigator.userAgent.match(/iPhone/i) ||
      navigator.userAgent.match(/iPad/i) ||
      navigator.userAgent.match(/iPod/i) ||
      navigator.userAgent.match(/BlackBerry/i) ||
      navigator.userAgent.match(/Windows Phone/i)
    ) {
      return true;
    } else {
      return false;
    }
  }
}
