export class Languages {
  static HEBREW   = 'he';
  static ENGLISH  = 'en';
  static RUSSIAN  = 'ru';
  static ROMAN    = 'ro';
  static CHINA    = 'zh_cn';
}
