import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

/**
 * FromNowPipe let's us convert a date into a human-readable relative-time
 * such as "10 minutes ago".
 */
@Pipe({
  name: 'fromNow'
})
export class FromNowPipe implements PipeTransform {
  transform(value: any, args: Array<any>): string {

    if (value) {
      if (! moment(value).isBefore(moment().subtract(1, 'd'))) {
        moment.locale('he');
        return moment(value).fromNow();
      } else {
        return 'ב' + moment(value).local().format('DD/MM/YY HH:MM');
      }
    } else {
      return value;
    }

  }
}

export const fromNowPipeInjectables: Array<any> = [
  FromNowPipe
];
