

// Get iframe parnet url
// Set server url according to iframe parent
let  serverUrl = (window.location !== window.parent.location)
  ? document.referrer // Parent url
  : document.location.href; // Set url accroding to current site url

// Get Hostname from server url (i dont need the full path)
const tempElement = document.createElement('a'); // Create a element
tempElement.href = serverUrl; // Set element url
serverUrl = tempElement.hostname + '/'; // Update server url


// Set socket io url according to server url
const dev_ws_url = 'https://js2.xfiinder.com';
const prod_ws_url = 'https://js.xfinder3.com';
const ws_url = serverUrl.includes('dev') ? dev_ws_url : prod_ws_url;


export const environment = {
  production: true,
  ws_url: ws_url,
  serverUrl: 'https://' + serverUrl + 'wp-json'
};
